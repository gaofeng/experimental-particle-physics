#include "MHSampling.h"
#include "Bootstrap.h"

double func2(double * x, double * para)
{
	double tau = para[0];
	return exp(-x[0]/tau)/tau;
}

void test_bs()
{
	double para[1] = {2.5};
	auto data1 = MHSampling::generate(func2, para, 10000, 0, 10);

	TH1F * h1 = new TH1F("h1", "", 100, 0, 10);
	for (auto x: data1) {
		h1->Fill(x);
	}
	h1->Draw();
	
	auto data2 = Bootstrap::resampling(data1);
	TH1F * h2 = new TH1F("h2", "", 100, 0, 10);
	for (auto x: data2) {
		h2->Fill(x);
	}
	h2->SetLineColor(2);
	h2->Draw("same");
}
