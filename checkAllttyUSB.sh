#/bin/sh
devices="0 1 2 3 4 5 6 7 8 9 10"
for device in ${devices}
do
    echo "check ttyUSB"${device}
    udevadm info /dev/ttyUSB${device} | grep  "P: "
done

unset devices
