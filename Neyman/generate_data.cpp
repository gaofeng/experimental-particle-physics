#include "MHSampling.h"
#include "Model.h"

void generate_data()
{
	TRandom3 RAND(0);
	double nbkg = 100;
	double nsig = 20;

	double xlow = 0;
	double xup = 3;
	Model::XLOW = xlow;
	Model::XUP = xup;

	double para[4] = {nsig, 1.9, 0.15, nbkg};
	auto data = MHSampling::generate(Model::BW_AND_FLAT, para, RAND.Poisson(nsig+nbkg), xlow, xup);

	TH1F * h = new TH1F("h1", "", 30, xlow, xup);
	for (auto x: data) {
		h->Fill(x);
	}
	h->Draw();

	ofstream os("data.txt");
	for (auto x: data) {
		os << x << endl;
	}
	os.close();
}
