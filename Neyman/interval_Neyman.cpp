#include "Model.h"
#include "Utils.h"
#include "MHSampling.h"

void interval_Neyman()
{
	double step = 0.5;
	const int nstep = 200;
	const int nmc = 2000;
	double n[nstep], alpha[nstep], beta[nstep];

	TRandom3 RAND(0);
	for (int i = 0; i < nstep; ++i) {
		cout << "constructing confidence belt: " << i+1 << " / " << nstep << endl;
		double nsig = 0 + i*step;
		double nbkg = 100;
		vector<double> arr_nsig;
		for (int j = 0; j < nmc; ++j) {
			double xlow = 0;
			double xup = 3;
			Model::XLOW = xlow;
			Model::XUP = xup;
			double para[4] = {nsig, 1.9, 0.15, nbkg};
			auto data = MHSampling::generate(Model::BW_AND_FLAT, para, RAND.Poisson(nsig+nbkg), xlow, xup);
		
			TH1F * h = new TH1F("h", "", 30, xlow, xup);
			fill_hist(data, h);
	
			TF1 * f = new TF1("f", Model::BW_AND_FLAT, xlow, xup, 4);
			f->SetParNames("nsig", "mass", "width", "nbkg");
			f->SetParameter("nsig", 10);
			f->SetParLimits(0, 0, h->GetEntries()); // nsig should be non-negative
			f->SetParameter("nbkg", 10);
			f->SetParLimits(3, 0, h->GetEntries()); // nbkg should be non-negative
			f->FixParameter(1, 1.9);  // mass
			f->FixParameter(2, 0.15);  // width
	
			h->Fit(f, "QL"); // Q: Quiet, L: Likelihood fit
			double bin_width = h->GetBinWidth(1);
			double nsig_fit = f->GetParameter("nsig") / bin_width;
			arr_nsig.push_back(nsig_fit);

			delete f;
			delete h;
		}
		sort(arr_nsig.begin(), arr_nsig.end());
		double a = arr_nsig[floor(nmc*0.159)];
		double b = arr_nsig[nmc-ceil(nmc*0.159)];
		alpha[i] = a;
		beta[i]  = b;
		n[i] = nsig;
	}

	TGraph * gr_a = new TGraph(nstep, n, alpha);
	TGraph * gr_b = new TGraph(nstep, n, beta);
	gr_b->Draw("alp");
	gr_a->Draw("lpsame");
}
