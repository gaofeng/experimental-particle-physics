#ifndef MHSAMPLING_H__
#define MHSAMPLING_H__

namespace MHSampling
{
	TRandom3 RAND;

	double transition_uniform(double xlast, double xlow, double xup, double & ratio)
	{
		double a = (xup-xlow)/4;
		double xnew;
		do {
			xnew = RAND.Uniform(xlast-a, xlast+a);
		} while (xnew < xlow || xnew > xup);
		
		double range1a = max(xlow, xlast-a);
		double range1b = min(xup,  xlast+a);
		double range2a = max(xlow, xnew-a);
		double range2b = min(xup,  xnew+a);
		double prob_last_to_new = 1.0/(range1b-range1a);
		double prob_new_to_last = 1.0/(range2b-range2a);
		ratio = prob_new_to_last / prob_last_to_new;
		
		return xnew;
	}
	
	double transition_normal(double xlast, double xlow, double xup, double & ratio)
	{
		double a = (xup-xlow)/4;
		double xnew;
		do {
			xnew = RAND.Gaus(xlast, a);
		} while (xnew < xlow || xnew > xup);
		
		double range1a = max(xlow, xlast-a);
		double range1b = min(xup,  xlast+a);
		double range2a = max(xlow, xnew-a);
		double range2b = min(xup,  xnew+a);
		double prob_last_to_new = TMath::Gaus(xnew, xlast, a) / (TMath::Erf((range1b-xlast)/a/sqrt(2)) - TMath::Erf((range1a-xlast)/a/sqrt(2)));
		double prob_new_to_last = TMath::Gaus(xlast, xnew, a) / (TMath::Erf((range2b-xnew)/a/sqrt(2)) - TMath::Erf((range2a-xnew)/a/sqrt(2)));
		ratio = prob_new_to_last / prob_last_to_new;
		
		return xnew;
	}
	
	vector<double> generate(function<double(double *, double *)> func, double * para, int n, double xlow, double xup, int nstart = -100)
	{
		vector<double> data;
		int niter = nstart;
		double xlast = (xlow+xup)/2;
		while (niter < n) {
			double gamma = 1;
			double xnew = transition_uniform(xlast, xlow, xup, gamma);
			//double xnew = transition_normal(xlast, xlow, xup, gamma);
			gamma *= func(&xnew, para) / func(&xlast, para);
			if (gamma > 1) {
				xlast = xnew;
			}
			else {
				double p = RAND.Rndm();
				if (gamma > p) {
					xlast = xnew;
				}
			}
			++niter;
	
			if (niter > 0) {
				data.push_back(xlast);
			}
		}
		return data;
	}
}

#endif
