#!/bin/sh

# enable python env and give read and execute for usb serial ports
source ~/Software/MUX64_QFN88_TEST/bin/activate
sudo chmod 777 -R /dev/bus/usb
sudo chmod 777 /dev/ttyUSB*

# unbind all ftdiso device from system (For UPL)
echo "Check serial attached by Linux kernel"
ls /sys/bus/usb/drivers/ftdi_sio/
# upl1
# unbind upl1
# echo -n 3-1.2:1.0 | sudo tee /sys/bus/usb/drivers/ftdi_sio/unbind
echo -n 2-1.2.4.1:1.0 | sudo tee /sys/bus/usb/drivers/ftdi_sio/unbind
# unbind upl2
echo -n 3-1.3:1.0 | sudo tee /sys/bus/usb/drivers/ftdi_sio/unbind
# UPL HERE???
export UPL1="0"
export UPL2="1"


# Define enviroment variable for all device
# Powersupply
export POWERSUPPLY="ASRL/dev/ttyUSB0::INSTR"
# Port for relay
export RELAY="/dev/ttyUSB0"

# Multimeters
# export MULTIMETER1="TCPIP0::192.168.10.14::inst0::INSTR"
# Another Multimeter
export MULTIMETER1="USB0::2391::1543::MY47027594::0::INSTR"
export MULTIMETER2="USB0::2391::1543::MY47027595::0::INSTR"

# Sourcemeters
# b2912a
export SOURCEMETER1="USB0::2391::36376::MY51140383::0::INSTR"
# keithley 2450 1
export SOURCEMETER2="USB0::1510::9296::04499139::0::INSTR"
# keithley 2450 2
export SOURCEMETER3="USB0::1510::9296::04113288::0::INSTR"
