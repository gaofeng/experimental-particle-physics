//================= user defined function =====================
double gaus(double * x, double * para) 
{
	double norm = para[0];
	double mu  = para[1];
	double sigma = para[2];
	
	double e = x[0]; // argument
	double z = (e-mu)/sigma;

	return norm*exp(-z*z/2)/sigma;
}

//================= acceptance-rejection sampling =====================
void fill_hist(TH1F * h, TF1 * f, double * para, int nfill = 5000, int seed = 100, int ntry = 1000) // nfill is the sample size
{
	double nbin = h->GetNbinsX();
	double xmin = h->GetBinLowEdge(1); // the histgram begins from 1, 0 is the left all, n+1 is the right all
	double xmax = h->GetBinLowEdge(nbin) + h->GetBinWidth(nbin);

	double fmax = 0;
	f->SetParameters(para);

	// get the maximum f(x) value by sampling
	TRandom3 rndm(seed);
	for (int i = 0; i < ntry; ++i) { // is the same with i++, but ++i consumes fewer resources
		double x = rndm.Uniform(xmin, xmax);
		double ftmp = f->Eval(x); // put out the dependent variable
		if (fmax < ftmp) fmax = ftmp;
	}
	fmax *= 1.2; // put out a position equal or greater than the maximum value 
	
	/* you can also try the TF1::GetMaximum(...) function
	   refer to https://root.cern.ch/doc/master/classTF1.html#adb9c2104b4877d97ce00e7f1e81085e5 for more details */
	//fmax = 1.2*f->GetMaximum(xmin, xmax);

	// steps:
	// (1) generate x in [ximn, xmax]
	// (2) generate rnd in [0, 1]
	//     then we get (x, rnd*1.2*fmax)
	// (3) if rnd*1.2*fmax < f(x), which is rnd < cut, accept
	int cnt = 0;
	while (cnt < nfill) {
		double x = rndm.Uniform(xmin, xmax);
		double cut = f->Eval(x)/fmax; // compare whether save or not
		double rnd = rndm.Rndm(); // uniform distribution between [0,1]
		if (rnd < cut) {
			h->Fill(x);
			++cnt;
		}
	}
}

//================= fit the every sample =====================
void gaus_hist_fit()
{
	TF1 * func = new TF1("f", gaus, 0, 1, 3); // in [0, 1], 3 means 3 parameter
	double para[3] = {1, 0.65, 0.2};
	func->SetParameters(para);
	func->Draw();
	
	int nfit = 10000;
	vector<double> mu, sigma;
		
	int nfill = 10000;
	int seed = 100;
	
	for (int i = 0; i < nfit; ++i) { // the sample size is 10000, we generate 10000 (also can don't 10000) times (means sample number is 10000) then fit and get mu and sigma 
		TH1F * h = new TH1F("h", "", 100, 0, 1); 
		fill_hist(h, func, para, nfill, seed);
		++seed;

		TF1 * fitf = new TF1("fitf", gaus, 0, 1, 3);
		double para_fit[3] = {1, 0.5, 0.1};
		fitf->SetParameters(para_fit);
		h->Fit(fitf); // if h->Fit(fitf, "R"); means fit in a range, "Q": quite mode, minimum output

		auto fitr = h->GetFunction("fitf"); // get the function after fitting
		double m = fitr->GetParameter(1);
		double s = fitr->GetParameter(2);

		mu.push_back(m);
		sigma.push_back(s);

		delete fitf;
		delete h;
	}

	double mu_max = *max_element(mu.begin(), mu.end()); // max_element is the function of C++, get the maximum
	double mu_min = *min_element(mu.begin(), mu.end());
	double sigma_max = *max_element(sigma.begin(), sigma.end());
	double sigma_min = *min_element(sigma.begin(), sigma.end());

	TH1F * h_mu = new TH1F("h_mu", "distribution of #mu's fitted value", 100, mu_min, mu_max);
	TH1F * h_sigma = new TH1F("h_sigma", "distribution of #sigma's fitted value", 100, sigma_min, sigma_max);

	for (auto m: mu) { // m is in mu, means mu[i]
		h_mu->Fill(m);
	}
	

	for (auto s: sigma) {
		h_sigma->Fill(s);
	}

	TCanvas * c = new TCanvas("c", "", 1200, 600);
	c->Divide(2, 1);

	c->cd(1);
	h_mu->Draw(); // "alp" means line points, "ap" means points

	c->cd(2);
	h_sigma->Draw();
}
