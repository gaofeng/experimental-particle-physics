#!/usr/bin/env python
#encoding: utf-8
# Company:  CAS IHEP
# Engineer:  zhj_at_ihep.ac.cn
# ref: https://github.com/ikuehirata/Agilent_B2912A_Controller

from sys import stderr
import numpy as np
import sys
import pyvisa
import time
import lib.relay_control
import lib.upl
from PowerSupply import PowerSupply, EasyPrint
from pyvisa_device import SourceMeter_b2912a, SourceMeter_2450, Multimeter_k3410a
from InteractiveMonitor import ReliabilityMonitor
from helper_function import MUX64PerpetualBroken, XrayRONTest, closeInstXray, TCNewTest_1, LOT_Test, getTimeStr
import os


def main():
    """
    Init all instrument first! Please make setup config file and config them!
    """
    # By custom FTDI2XX driver. UI in ./lib/upl.py
    # Initialize and setup relay
    # print("Init UPL1")
    upl1_name = os.getenv("UPL1")
    # print(upl1_name)
    upl1 = lib.upl.upl(dev=int(upl1_name), i2c_freq=100, mux64_test=1)
    upl1.mux64_select(0)

    # print("Init UPL2")
    # upl2_name = os.getenv("UPL2")
    # print(upl2_name)
    # upl2 = lib.upl.upl(dev=int(upl2_name), i2c_freq=100, mux64_test=1)
    # upl2.mux64_select(0)
    upl2 = None

    # By os ftdi_sio driver. UI in PowerSupply.py
    # Initialize and setup GPD-4303s power supply
    # power_name = os.getenv("POWERSUPPLY")
    # print(power_name)
    # powersupply1 = PowerSupply(power_name)
    # powersupply1.SetVoltage(1, 12)
    # powersupply1.Output("ON")
    # EasyPrint(powersupply1)
    powersupply1 = None

    # By serial-to-modbus RS-485 port. UI in ./lib/relay.py
    # The name here exactly usb serial name
    # Initialize and setup relay.
    relay_name = os.getenv("RELAY")
    # print(relay_name)
    try:
        relay1 = lib.relay_control.relay(relay_name)
        # print("Close all input channels")
        relay1.channel_all_off()
    except Exception as e:
        print(e)
        relay1 = None

    # Initialize device control by pyvisa-py
    # pyvisa.ResourceManager
    rm = pyvisa.ResourceManager()
    # Command to print all available visa resouces
    rm.list_resources()

    # Init multimeter
    multimeter1 = Multimeter_k3410a(os.getenv("MULTIMETER1"))
    multimeter1.OpenResource()
    multimeter1.ConfigureVoltageMeasurement()

    # multimeter2 = Multimeter_k3410a(os.getenv("MULTIMETER2"))
    # multimeter2.OpenResource()
    # multimeter2.ConfigureVoltageMeasurement()
    multimeter2 = None

    # Init sourcemeter
    # b2912a with 2 channel

    sourcemeter1 = SourceMeter_b2912a(os.getenv("SOURCEMETER1"))
    sourcemeter1.OpenResource()
    sourcemeter1.ConfigB2912a()
    
     # 2450 with 1 channel
    # sourcemeter2 = SourceMeter_2450(os.getenv("SOURCEMETER2"))
    # sourcemeter2.OpenResource()
    # sourcemeter2.Config2450()
    sourcemeter2 = None
    # sourcemeter3 = SourceMeter_2450(os.getenv("SOURCEMETER3"))
    # sourcemeter3.OpenResource()
    # sourcemeter3.Config2450()
    sourcemeter3 = None

    # Init the interactive monitor
    # Aim to plot R_ON in realtime
    one_fig_monitor = ReliabilityMonitor(1, 2)

    try:
        # Perform the test for once here!
        # XrayRONTest(sourcemeter1, sourcemeter2, sourcemeter3, multimeter1, multimeter2, powersupply1, relay1, upl1, upl2,
        #            measure_involtages=np.linspace(0.0, 1.2, 25)[1:], measure_times=2, acquisition_delay=0.05, postfix="XrayTest0", fig_monitor=one_fig_monitor)
        # print("\nMeasurement started!")
        LOT_Test(sourcemeter1, sourcemeter2, sourcemeter3, multimeter1, multimeter2, powersupply1, relay1, upl1, upl2,
                measure_involtages=np.linspace(0.0, 1.2, 7)[1:], measure_times=1, acquisition_delay=0.05, postfix="P2N0386", fig_monitor=one_fig_monitor)
        # print("Measurement ended!")

    except KeyboardInterrupt:
        print()
        print(
            "Shutdown from keyboard...... Wait a seconds to close the inst", file=sys.stderr)
    except MUX64PerpetualBroken as e:
        print()
        print("MUX64 {0} channel {1} (first discoverd) is broken, shutdown now...... wait a seconds to clean the mess", file=sys.stderr)
        print("Broken time at: {0}".format(
            str(time.strftime("%Y-%m-%d_%Hh%Mm%Ss"))), file=sys.stderr)

    # end of session beep
    time.sleep(.5)

    """
    Close all your device here!
    Write your own function. For example: closeInstXray(....)
    Please always close the device in this file!
    """
    closeInstXray(upl1, upl2, relay1, powersupply1, multimeter1,
                  multimeter2, sourcemeter1, sourcemeter2, sourcemeter3)
    # closeInstSingleSet(powersupply1=None, relay1=None,
    #                    upl1=upl1, k34410a=k34410a)
    time.sleep(1.)


if __name__ == "__main__":
    main()
