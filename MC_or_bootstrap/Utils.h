#ifndef UTILS_H__
#define UTILS_H__

vector<double> read_data(TString fname)
{
	vector<double> data;
	ifstream is(fname);
	double x;
	while (is >> x) {
		data.push_back(x);
	}
	return data;
}

void fill_hist(vector<double> & data, TH1 * h)
{
	for (auto x: data) {
		h->Fill(x);
	}
}

#endif
