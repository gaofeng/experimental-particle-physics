#include "Model.h"
#include "Utils.h"
#include "Bootstrap.h"

void interval_bs()
{
	auto data = read_data("data.txt");
	double xlow = Model::XLOW;
	double xup = Model::XUP;
	
	int ntest = 1000;
	vector<double> arr_nsig;
	for (int i = 0; i < ntest; ++i) {
		auto data_bs = Bootstrap::resampling(data, true);
		TH1F * h = new TH1F("h", "", 30, xlow, xup);
		fill_hist(data_bs, h);
	
		TF1 * f = new TF1("f", Model::BW_AND_FLAT, xlow, xup, 4);
		f->SetParNames("nsig", "mass", "width", "nbkg");
		f->SetParameter("nsig", 10);
		f->SetParLimits(0, 0, h->GetEntries()); // nsig should be non-negative
		f->SetParameter("nbkg", 10);
		f->SetParLimits(3, 0, h->GetEntries()); // nbkg should be non-negative
		f->FixParameter(1, 1.9);  // mass
		f->FixParameter(2, 0.15);  // width
	
		h->Fit(f, "QL"); // Q: Quiet, L: Likelihood fit
		double bin_width = h->GetBinWidth(1);
		double nsig_fit = f->GetParameter("nsig") / bin_width;
		arr_nsig.push_back(nsig_fit);

		delete f;
		delete h;
	}
	sort(arr_nsig.begin(), arr_nsig.end());

	double n1 = arr_nsig[floor(ntest*0.159)];
	double n2 = arr_nsig[ntest-ceil(ntest*0.159)];
	cout << "estimated interval: [" << n1 << ", " << n2 << "]" << endl;
}
