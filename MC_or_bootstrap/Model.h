#ifndef MODEL_H__
#define MODEL_H__

namespace Model
{
	double XLOW = 0;
	double XUP = 3;
	
	double bw_norm(double x, double m, double w, double xlow, double xup)
	{
		double norm = (w/2) / (atan(2*(xup-m)/w) - atan(2*(xlow-m)/w));
		return norm / ((m-x)*(m-x)+w*w/4);
	}
	
	double uniform_norm(double x, double xlow, double xup)
	{
		return 1.0 / (xup-xlow);
	}
	
	double BW_AND_FLAT(double * x, double * para)
	{
		// sig: breit-wigner
		double nsig = para[0];
		double m = para[1];
		double w = para[2];
		double fsig = bw_norm(x[0], m, w, XLOW, XUP);
	
		// bkg: uniform
		double nbkg = para[3];
		double fbkg = uniform_norm(x[0], XLOW, XUP);
	
		return nsig*fsig + nbkg*fbkg;
	}
}

#endif
