#include "Model.h"
#include "Utils.h"

void interval_MC()
{
	double xlow = Model::XLOW;
	double xup = Model::XUP;
	TH1F * h = new TH1F("h", "", 100, xlow, xup);
	auto data = read_data("data.txt");
	fill_hist(data, h);
	
	TF1 * f = new TF1("f", Model::BW_AND_FLAT, xlow, xup, 4);
	
	f->SetParNames("nsig", "mass", "width", "nbkg");
	f->SetParameter("nsig", 10);
	f->SetParLimits(0, 0, h->GetEntries()); // nsig should be non-negative
	f->SetParameter("nbkg", 10);
	f->SetParLimits(3, 0, h->GetEntries()); // nsig should be non-negative
	f->FixParameter(1, 1.9);  // mass
	f->FixParameter(2, 0.15);  // width
	
	h->Fit(f, "L");
	double bin_width = h->GetBinWidth(1);
	double nsig_fit = f->GetParameter("nsig") / bin_width;
	double nsig_fit_err = f->GetParError(0) / bin_width;
	cout << "nsig_fit = " << nsig_fit << " +/- " << nsig_fit_err << endl;
}
