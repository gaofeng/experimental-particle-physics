#ifndef BOOTSTRAP_H__
#define BOOTSTRAP_H__

namespace Bootstrap
{
	TRandom3 RAND(0);
	vector<double> resampling(vector<double> & data, bool random_nevt = false)
	{
		int n = data.size();
		int nevt = random_nevt ? RAND.Poisson(n) : n;

		vector<double> data_new;
		for (int i = 0; i < nevt; ++i) {
			int id = floor(n*RAND.Rndm());
			double xnew = data[id];
			data_new.push_back(xnew);
		}
		return data_new;
	}
}

#endif
