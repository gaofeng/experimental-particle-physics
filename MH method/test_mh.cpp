#include "MHSampling.h"

double func(double * x, double * para)
{
	double s = x[0]*x[0];
	double m = para[0];
	double w = para[1];
	return m*w/(pow(m*m-s,2)+m*m*w*w);
}

void test_mh()
{
	double para[2] = {1.5, 0.1};
	auto data = MHSampling::generate(func, para, 10000, 1, 2);

	TH1F * h = new TH1F("h1", "", 100, 1, 2);
	for (auto x: data) {
		h->Fill(x);
	}
	h->Draw();
}
