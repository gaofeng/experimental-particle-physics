class Bernoulli
{
	public:
		Bernoulli(double p = 0.5);
		virtual ~Bernoulli() {}
		void set_prob(double p) { _p = p; }
		int val();

	private:
		double _p; // _ just show it's private
		static TRandom3 rndm;
}; //<-- do not forget this semicolon

Bernoulli::Bernoulli(double p):
	_p(p)
{
	if (p<=0 || p>=1) {
		cout << "'p' should not be smaller than 0 or larger than 1; reset to 0.5." << endl;
		_p = 0.5;
	}
}

int Bernoulli::val()
{
	if (rndm.Rndm() < _p) return 1; // rndm.Rndm() return uniform distribution random number
	else return 0;
}

TRandom3 Bernoulli::rndm; // TRandom3=int, this is a statement, rndm belows to Bernoulli not TRandom3

void verify_clt()
{
	const int N = 10;
	double p = 0.3; // Bernoulli(0.3)
	// perpare 10 i.i.d. random variables ~ Bernoulli(0.3)
	Bernoulli X[N];
	for (int i = 0; i < N; ++i) {
		X[i].set_prob(p);
	}

	TH1F * h = new TH1F("h", "sum of 10 Bernoulli(0.3)", 110, -0.5, 10.5);
	int ntest = 1000;
	for (int i = 0; i < ntest; ++i) {
		int sum = 0;
		for (int j = 0; j < N; ++j) {
			sum += X[j].val();
		}
		h->Fill(sum);
	}
	h->Draw();
}
