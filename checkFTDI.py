import ftd2xx as ftd
import sys
import time
import lib.upl

num_devices = ftd.createDeviceInfoList()
if (num_devices == 0):
    print("No device found.")
print("Found %d devices."%num_devices)

for i in range(0, num_devices):
    try:
        device = lib.upl.upl(dev=i, i2c_freq=10, mux64_test=18)
        for j in range(0, 64):
            print("select {0}".format(j))
            device.mux64_select(j)
            time.sleep(1)
        device.close()
        # time.sleep(2.5)
    except ftd.ftd2xx.DeviceError as e: 
        print("Device {0} is not opened".format(i))
        print(e)
        continue
    
    print("Device{0} is open".format(i))
    # print(device.getDeviceInfo())
